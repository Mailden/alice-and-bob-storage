### Bob&Alice storage system
> Securely store, retrieve and search your digital goods


**Bob** stands for « Box Of Bytes ».
**Alice** stands for « Asynchronous Lexicon and Index for Content Exploration »

Bob&Alice is a software solution to store user's digital goods within fully encrypted silos, while still offering search capabilities **when user is connected**.

« digital goods » are emails/messages, address books, calendars, etc... All the stuffs that we constantly need to access and search by many means and softwares : webmail, messaging application, smartphone's calendar app, etc. via protocols like IMAP/JMAP, cardDAV/calDAV, etc.

###### Bob&Alice implements a _two states_ storage system for digital goods :
* **letterbox state**. User is not logged. Only PUT operations available to add documents to silo, with on-the-fly encryption.
* **open state**. User is logged in. Its private PGP key is loaded into memory. Read/write operations available. Asynchronous indexation starts. Search API available.

Bob&Alice would be a solution to store digital goods fully encrypted at rest, while still allowing user to search inside its documents when connected. This would be done by loading a personal index/database when user is logged to the service, to expose a searh API.
Upon user's disconnection, documents, database and index would be instantly locked back within encrypted blobs.

see [General picture](General picture-2019-01-29.pdf)